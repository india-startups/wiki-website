# India Startup Playbooks Website

This is the Hugo powered website [hosted on Gitlab Pages](https://india-startups.gitlab.io/wiki-website/) for the [Playbooks Wiki](https://gitlab.com/india-startups/playbooks-wiki), which is a git + markdown based wiki hosted on Gitlab.


## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Clone this project 
1. Init and update submodules
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.
